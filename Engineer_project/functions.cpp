/**
*\file
*\brief Сами функции подсчета
*\author Igor K.
*\version 3.5.6.0
*\date 07.06.2019
*/
#include "functions.h"
/**
    \brief Функция позволяет посчитать функцию экспоненты


    \param[in] x Степень экспоненты
    \param[in] e Точность счета

    Исходный код:
    \code
    double our_Exp(double x, double e)
    {
        double n = 1;
        double sum = 0.0;
        int i = 1;

        do
        {
            sum += n;
            n *= x / i;
            i++;
        } while (fabs(n) > e);

        return sum;
    }
    \endcode
*/
double our_Exp(double x, double e)
{
    double n = 1;
    double sum = 0.0;
    int i = 1;

    do
    {
        sum += n;
        n *= x / i;
        i++;
    } while (fabs(n) > e);

    return sum;
}

double our_Sin(double x, double e)
{
    double n = x;
    double sum = 0.0;
    int i = 1;

    do
    {
        sum += n;
        n *= -1.0 * x * x / ((2 * i) * (2 * i + 1));
        i++;
    } while (fabs(n) > e);

    return sum;
}

double our_Cos(double x, double e)
{
    double n = 1.0;
    double sum = 0.0;
    int i = 1;

    do
    {
        sum += n;
        n *= -1.0 * x * x / ((2 * i - 1) * (2 * i));
        i++;
    } while (fabs(n) > e);

    return sum;
}

double our_Nlog(double x, double e)
{
    // Сделать проверку на ввод значений!
    double n = x;
    double sum = 0.0;
    int i = 2;

    do
    {
        sum += n;
        n *= -1.0 * x * (i - 1) / i;
        i++;

    } while (fabs(n) > e);

    return sum;
}

double our_Alpha(double x, double e, int a)
{
    // Сделать проверку на ввод значений!
    double n = 1;
    double sum = 0.0;
    int i = 1;

    do
    {
        sum += n;
        n *= (a - i + 1) * x / i;
        i++;
    } while (fabs(n) > e);

    return sum;
}

double our_Arctan(double x, double e)
{
    double n = x;
    double sum = 0.0;
    int i = 1;

    do
    {
        sum += n;
        n *= -1.0 * x * x / (i + 2) * i;
        i += 2;

    } while (fabs(n) > e);

    return sum;
}
