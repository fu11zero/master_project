#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include <cmath>

double our_Exp(double x, double e);
double our_Sin(double x, double e);
double our_Cos(double x, double e);
double our_Nlog(double x, double e);
double our_Alpha(double x, double e, int a);
double our_Arctan(double x, double e);

#endif // FUNCTIONS_H
