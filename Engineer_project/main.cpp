/**
*\file
*\brief Программа для подсчета различных математических функций с помощью разложения в ряд Маклорена
*
*В частности считает sin, cos, arctan.
*Код программы целиком представлен в примере.
*\author Igor K.
*\version 3.5.6.0
*\date 07.06.2019
*/
#include <QCoreApplication>
#include <QTextStream>
#include "functions.h"
#include <windows.h>

// Добавление комментария №1
// Добавление комментария №2
// Добавление комментария №3
int main(int argc, char *argv[])
{
    QTextStream cout(stdout);
    QTextStream cin (stdin);
    QCoreApplication a(argc, argv);

    double accuracy = 0.0001;
    bool help = true;
    while(help)
    {
        int x = 0;
        double result = 0.0;
        double number = 0.0;
        cout << "****Select a menu item****" << endl;
        cout << "0 - exit from programm" << endl;
        cout << "1 - change accuracy" << endl;
        cout << "2 - counting exhibitors e^x" << endl;
        cout << "3 - counting sin(x)" << endl;
        cout << "4 - counting cos(x)" << endl;
        cout << "5 - counting natural logarithm ln(1 + x)" << endl;
        cout << "6 - counting grade function (1 + x)^a"<< endl;
        cout << "7 - counting arctan(x)" << endl;
        cout << "Your choise: " << flush;
        cin >> x;

        system("cls");
        if (x == 0)
        {
            help = false;
            a.quit();
        }
        else if (x == 1)
        {
            QString answer = "";
             cout << "default accuracy: 0.0001" << endl;
             cout << "current accuracy: " << accuracy << endl;
             do
             {
                 answer = "";
                 cout << "change accuracy? yes|no: " << flush;
                 cin >> answer;
             } while ( answer != "no" && answer != "yes");
             if (answer == "yes")
             {
                 cout << "new accuracy: " << flush;
                 cin >> accuracy;
             }
             system("cls");
        }
        else if (x == 2)
        {
            cout << "counting exhibitors e^x" << endl;
            cout << "Enter degree: " << flush;
            cin >> number;
            result = our_Exp(number, accuracy);
            cout << "the value of exponential function in degree " << number << flush;
            cout << " with an accuracy of " << accuracy << " is " << result << endl;
             cout << "in short: e^" << number << " = " << result << endl;
            system("pause");
        }
        else if (x == 3)
        {
            cout << "counting sin(x)" << endl;
            cout << "Enter argument: " << flush;
            cin >> number;
            result = our_Sin(number,accuracy);
            cout << "the value of sin with argument = " << number << flush;
            cout << " and with an accuracy of " << accuracy << " is " << result << endl;
             cout << "in short: sin(" << number << ") = " << result << endl;
            system("pause");
        }
        else if (x == 4)
        {
            cout << "counting cos(x)" << endl;
            cout << "Enter argument: " << flush;
            cin >> number;
            result = our_Sin(number,accuracy);
            cout << "the value of cos with argument = " << number << flush;
            cout << " and with an accuracy of " << accuracy << " is " << result << endl;
             cout << "in short: cos(" << number << ") = " << result << endl;
            system("pause");
        }
        else if (x == 5)
        {
            cout << "counting natural logarithm ln (1 + x)" << endl;
            do
            {
                cout << "Enter x within (-1;1] : " << flush;
                cin >> number;
            } while ( number <= -1 || number > 1);
            result = our_Nlog(number,accuracy);
            cout << "the value of natural logarithm with argument = " << number + 1 << flush;
            cout << " and with an accuracy of " << accuracy << " is " << result << endl;
            cout << "in short: ln(" << number + 1 << ") = " << result << endl;
            system("pause");
        }
        else if (x == 6)
        {
            int alpha = 0;
            cout << "counting grade function (1 + x)^a" << endl;
            do
            {
                cout << "Enter x within (-1;1) : " << flush;
                cin >> number;
            } while ( number <= -1 || number >= 1);
            cout << "Enter grade(alpha): " << flush;
            cin >> alpha;
            result = our_Alpha(number,accuracy,alpha);
            cout << "the value of grade function with argument = " << number + 1 << " in grade = " << alpha << flush;
            cout << " and with an accuracy of " << accuracy << " is " << result << endl;
            cout << "in short: " << number + 1 << "^" << alpha << " = " << result << endl;
            system("pause");
        }
        else if (x == 7)
        {
            cout << "counting arctan(x)" << endl;
            do
            {
                cout << "Enter x within [-1;1] : " << flush;
                cin >> number;
            } while ( number <= -1 || number >= 1);
            result = our_Arctan(number,accuracy);
            cout << "the value of arctan with argument = " << number << flush;
            cout << " and with an accuracy of " << accuracy << " is " << result << endl;
            cout << "in short: arctan(" << number << ") = " << result << endl;
            system("pause");
        }
    }
    return 0;
    //return a.exec();
}
