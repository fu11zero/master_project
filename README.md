# Master_project
## Проектная работа по ВвИДу

Проектная работа включает в себя два проекта: юнит-тесты для различных функций и программу по подсчету этих функций с помощью рядов Маклорена, таких как:
* sin(x)
* cos(x)
* arctan(x)
* ln(1+x)
* (1+x)^a
* e^x

![alt Ряды Маклорена](http://www.webmath.ru/poleznoe/images/diff/razlozhenie_v_rjad_maklorena_2041.png)