QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app
INCLUDEPATH += /Engineer_project/
SOURCES +=  tst_engtest.cpp \
    main.cpp \
    ../Engineer_project/functions.cpp

HEADERS += \
    ../Engineer_project/functions.h \
    tst_engtest.h
