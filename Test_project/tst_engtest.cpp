/**
*\file
*\brief Тесты для наших функций
*\author Igor K.
*\version 1.0
*\date 07.06.2019
*/
#include <QtTest>
#include "../Engineer_project/functions.h"
#include "tst_engtest.h"
// add necessary includes here

bool equally(double x1, double x2, double acc)
{
    if (fabs(x1 - x2) < acc)
    {
        return true;
    }
    return false;
}
Engtest::Engtest()
{

}

/**
    \brief Проверка функции подсчета экпоненты

    Исходный код:
    \code
    void Engtest::TExp()
    {
        double accuracy = 0.0001;
        double result = our_Exp(3, accuracy);
        QCOMPARE(true, equally(result,20.0855369232,accuracy));

         accuracy = 0.001;
        result = our_Exp(4,accuracy);
        QCOMPARE(true, equally(result,54.598,accuracy));
    }
    \endcode
    */
void Engtest::TExp()
{
    double accuracy = 0.0001;
    double result = our_Exp(3, accuracy);
    QCOMPARE(true, equally(result,20.0855369232,accuracy));

    accuracy = 0.001;
    result = our_Exp(4,accuracy);
    QCOMPARE(true, equally(result,54.598,accuracy));
}

void Engtest::Tcos()
{
    double accuracy = 0.0001;
    double result = our_Cos(6,accuracy);
    QCOMPARE(true,equally(result,0.9601,accuracy));

    accuracy = 0.001;
    result = our_Cos(15,accuracy);
    QCOMPARE(true,equally(result,-0.759,accuracy));
}

void Engtest::Tsin()
{
    double accuracy = 0.0001;
    double result = our_Sin(5,accuracy);
    QCOMPARE(true,equally(result,-0.9589,accuracy));

    accuracy = 0.001;
    result = our_Sin(4,accuracy);
    QCOMPARE(true,equally(result,-0.756,accuracy));
}

void Engtest::Tarctan()
{
    double accuracy = 0.0001;
    double result = our_Arctan(0.25,accuracy);
    QCOMPARE(true,equally(result,0.2449,accuracy));

    accuracy = 0.01;
    result = our_Arctan(1,accuracy);
    QCOMPARE(false,equally(result,0.8,accuracy)); // Тут ложь!
}

void Engtest::Tnlog()
{
    double accuracy = 0.0001;
    double result = our_Nlog(0.5,accuracy);
    QCOMPARE(true,equally(result,0.4055,accuracy));

    accuracy = 0.001;
    result = our_Nlog(1,accuracy);
    QCOMPARE(false,equally(result,0.691,accuracy)); // Тут ложь!
}

void Engtest::Talpha()
{
    double accuracy = 0.0001;
    double result = our_Alpha(0.5,accuracy, 3);
    QCOMPARE(true,equally(result,3.375,accuracy));

    accuracy = 0.001;
    result = our_Alpha(1,accuracy, 10);
    QCOMPARE(true,equally(result,1024,accuracy));
}


