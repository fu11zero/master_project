#ifndef TST_ENGTEST_H
#define TST_ENGTEST_H
#include <QtCore>
#include <QtTest/QtTest>


class Engtest : public QObject
{
    Q_OBJECT

public:
    Engtest();

private slots:
        void TExp();
        void Tsin();
        void Tcos();
        void Tnlog();
        void Tarctan();
        void Talpha();

};
#endif // TST_ENGTEST_H
